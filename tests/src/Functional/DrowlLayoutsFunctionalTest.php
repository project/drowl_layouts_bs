<?php

namespace Drupal\Tests\drowl_layouts_bs\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * This class provides methods specifically for testing something.
 *
 * @group drowl_layouts_bs
 */
class DrowlLayoutsFunctionalTest extends BrowserTestBase {
  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'field',
    'field_ui',
    'test_page_test',
    'layout_discovery',
    'layout_builder',
    'layout_options',
    'drowl_layouts_bs',
    'twig_real_content',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->config('system.site')->set('page.front', '/test-page')->save();
    $this->user = $this->drupalCreateUser([]);
    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests if the module installation, won't break the site.
   */
  public function testInstallation() {
    $session = $this->assertSession();
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
  }

  /**
   * Tests if uninstalling the module, won't break the site.
   */
  public function testUninstallation() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $this->drupalGet('/admin/modules/uninstall');
    $session->statusCodeEquals(200);
    $page->checkField('edit-uninstall-drowl-layouts-bs');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    // Confirm deinstall:
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->pageTextContains('The selected modules have been uninstalled.');
  }

  /**
   * Tests to see if the layouts are available in layout_builder.
   */
  public function testLayoutsExist() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $this->createContentType(['type' => 'article']);
    $this->drupalGet('/admin/structure/types/manage/article/display');
    $session->statusCodeEquals(200);
    $page->fillField('edit-layout-enabled', 1);
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $this->drupalGet('/layout_builder/choose/section/defaults/node.article.default/0');
    $session->statusCodeEquals(200);

    $session->pageTextContains('Node Detail Default');
    $session->pageTextContains('Media Object (Media on the left side)');
    $session->pageTextContains('One column (Stacked)');
    $session->pageTextContains('Two column (Stacked)');
    $session->pageTextContains('Three column (Stacked)');
    $session->pageTextContains('Four column (Stacked)');
    $session->pageTextContains('Five column (Stacked)');
    $session->pageTextContains('Six column (Stacked)');
    $session->pageTextContains('One column');
    $session->pageTextContains('Two column');
    $session->pageTextContains('Three column');
    $session->pageTextContains('Four column');
    $session->pageTextContains('Five column');
    $session->pageTextContains('Six column');

    // Since we are not prefixing the layouts anymore, we need to check, that
    // we are not accidentally asserting the core layouts instead. Meaning we
    // need to check the "simple" layouts via css selector:
    $session->elementExists('css', 'svg.layout-icon--drowl-layouts-bs-1col');
    $session->elementExists('css', 'svg.layout-icon--drowl-layouts-bs-2col');
    $session->elementExists('css', 'svg.layout-icon--drowl-layouts-bs-3col');
    $session->elementExists('css', 'svg.layout-icon--drowl-layouts-bs-4col');
    $session->elementExists('css', 'svg.layout-icon--drowl-layouts-bs-5col');
    $session->elementExists('css', 'svg.layout-icon--drowl-layouts-bs-6col');
  }

  /**
   * Tests using a layout as display and creating a node.
   */
  public function testUsingLayout() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $this->createContentType(['type' => 'article']);
    $this->drupalGet('/admin/structure/types/manage/article/display');
    $session->statusCodeEquals(200);
    $page->fillField('edit-layout-enabled', 1);
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $this->drupalGet('/layout_builder/configure/section/defaults/node.article.default/0/drowl_layouts_bs_node_detail_default');
    $session->statusCodeEquals(200);
    $page->fillField('edit-layout-settings-label', 'test_label');
    $page->pressButton('edit-actions-submit');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Configure test_label');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->pageTextContains('The layout has been saved.');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Your settings have been saved.');

    $this->drupalGet('/node/add/article');
    $session->statusCodeEquals(200);
    $page->fillField('edit-title-0-value', 'test');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->pageTextContains('article test has been created.');
  }

}
